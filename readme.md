# Introduction to Identity on ASP.NET Core

Referans : https://docs.microsoft.com/en-us/aspnet/core/security/authentication/identity?view=aspnetcore-2.1&tabs=netcore-cli

# Step 1

[ dotnet new webapp --auth Individual -o JobTracker ]

* Select File > New > Project.
* Select ASP.NET Core Web Application. Name the project akshaybhagwat to have the same namespace as the project download. Click OK.
* Select an ASP.NET Core Web Application, then select Change Authentication.
* Select Individual User Accounts and click OK.

```DB
    //"DefaultConnection": "DataSource=app.db",
    //"DefaultConnection": "Server=(localdb)\\mssqllocaldb;Database=aspnet-JobTracker;Trusted_Connection=True;MultipleActiveResultSets=true"
    //"DefaultConnection": "Server=.;Database=aspnet-JobTracker;Trusted_Connection=True;MultipleActiveResultSets=true"
```

# Step 2
Package Manager Console (PMC):
PM> Update-Database 

or 

.NET Core CLI
[ dotnet ef database update }

# Step 3
startup.cs file change for Configure Identity Services

```c#

public void ConfigureServices(IServiceCollection services)
{
    .
    .
    .

    // Configure Identity services
    services.Configure<IdentityOptions>(options =>
    {
        // Password settings.
        options.Password.RequireDigit = false;
        options.Password.RequireLowercase = false;
        options.Password.RequireNonAlphanumeric = false;
        options.Password.RequireUppercase = false;
        options.Password.RequiredLength = 6;
        options.Password.RequiredUniqueChars = 1;

        // Lockout settings.
        options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
        options.Lockout.MaxFailedAccessAttempts = 5;
        options.Lockout.AllowedForNewUsers = true;

        // User settings.
        options.User.AllowedUserNameCharacters =
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
        options.User.RequireUniqueEmail = false;
    });

    .
    .
    .

}
```

# step 4

Application cooike

startup.cs file change for Configure Identity Services

```c#
public void ConfigureServices(IServiceCollection services)
{
    .
    .
    .

    // Application Cooike
    services.ConfigureApplicationCookie(options =>
    {
        // Cookie settings
        options.Cookie.HttpOnly = true;
        options.ExpireTimeSpan = TimeSpan.FromMinutes(5);

        options.LoginPath = "/Identity/Account/Login";
        options.AccessDeniedPath = "/Identity/Account/AccessDenied";
        options.SlidingExpiration = true;
    });
    .
    .
    .

}
```

# step 5
```console
dotnet add package Microsoft.EntityFrameworkCore
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
dotnet tool install --global dotnet-aspnet-codegenerator
dotnet aspnet-codegenerator identity -dc JobTracker.Data.ApplicationDbContext --files "Account.Register;Account.Login;Account.Logout"
```

# step 6
Add Data Table Class
```
namespace JobTracker.Data
{
    public class AccountNumber
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string Description { get; set; }
    }
}

```

Add a new Controller With Entity Framework

New Data Table

```
  PM> Add-Migration AccountNumbers
  PM> Update-Database

```

# step 7
```` 
    [Authorize]
    public class AccountNumbersController : Controller
```