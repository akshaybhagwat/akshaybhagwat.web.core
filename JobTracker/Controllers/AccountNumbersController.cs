﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using JobTracker.Data;
using Microsoft.AspNetCore.Authorization;

namespace JobTracker.Controllers
{
    
    public class AccountNumbersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AccountNumbersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: AccountNumbers
        // Everyone acces
        public async Task<IActionResult> Index()
        {
            return View(await _context.AccountNumbers.ToListAsync());
        }

        // GET: AccountNumbers/Details/5
        // Only Administrator Roles Details
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var accountNumber = await _context.AccountNumbers
                .FirstOrDefaultAsync(m => m.ID == id);
            if (accountNumber == null)
            {
                return NotFound();
            }

            return View(accountNumber);
        }

        // GET: AccountNumbers/Create
        // Only Authorize users
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        // POST: AccountNumbers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        // Only Authorize User
        [Authorize]
        public async Task<IActionResult> Create([Bind("ID,Name,Number,Description")] AccountNumber accountNumber)
        {
            if (ModelState.IsValid)
            {
                _context.Add(accountNumber);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(accountNumber);
        }

        // GET: AccountNumbers/Edit/5
        // Only Authorize User
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var accountNumber = await _context.AccountNumbers.FindAsync(id);
            if (accountNumber == null)
            {
                return NotFound();
            }
            return View(accountNumber);
        }

        // POST: AccountNumbers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        // Administrator Role Policy Based
        [Authorize(Policy = "RequireAdministratorRole")]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Number,Description")] AccountNumber accountNumber)
        {
            if (id != accountNumber.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(accountNumber);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AccountNumberExists(accountNumber.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(accountNumber);
        }

        // GET: AccountNumbers/Delete/5
        // Multiple Policy Claim Based
        [Authorize(Policy = "Founders")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var accountNumber = await _context.AccountNumbers
                .FirstOrDefaultAsync(m => m.ID == id);
            if (accountNumber == null)
            {
                return NotFound();
            }

            return View(accountNumber);
        }

        // POST: AccountNumbers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        // Multiple Policy Claim Based
        [Authorize(Policy = "Founders")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var accountNumber = await _context.AccountNumbers.FindAsync(id);
            _context.AccountNumbers.Remove(accountNumber);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AccountNumberExists(int id)
        {
            return _context.AccountNumbers.Any(e => e.ID == id);
        }
    }
}
